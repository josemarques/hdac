Library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_misc.all;
--use ieee.std_logic_arith.all;
--use ieee.std_logic_signed.all;
entity R2RSDAC is
	generic(	Ni: integer :=16;--Number of bits of HDAC
				No: integer :=8--Number of bits of R2RDAC
				);
	port(	DATA : IN std_logic_vector (Ni-1 DOWNTO 0);--Input data
			PWMS:	OUT std_logic_vector ((Ni-No) DOWNTO 0);--PWM data signal
			R2R : OUT std_logic_vector (No-1 DOWNTO 1)--R2R signal. (No) bits less one that's going to be PWMemd
	    );
end R2RSDAC;
--
architecture R2R_R2RSDAC of R2RSDAC is

begin
	R2RGEN: for i in 1 to (No-1) generate
		R2R(i) <= DATA(Ni-1-(No-1)+i);--Pass the most significant bits from data to R2R
	end generate R2RGEN;
	
	PWMSGEN: for i in 0 to (Ni-No) generate
		PWMS(i) <= DATA(i);--Pass the less significant bits from data to PWMS
	end generate PWMSGEN;

end R2R_R2RSDAC;
--BUFFER_REG_N--

--architecture PWM_CLOCK_DIVIDER of DPWM is
--	signal COUNT: std_logic_vector (N-1 DOWNTO 0):=(OTHERS=>'0');
--	signal MAX: std_logic_vector (N-1 DOWNTO 0):=(OTHERS=>'1');
--
--begin
--
--	process(CLK)
--	begin
--			if(CLK'event and CLK='1')then
--				COUNT  <= std_logic_vector(unsigned(COUNT)+1);
--			end if;
--		
--	end process;
--	
--	--PWM out signal
--	PWM<=	'1' WHEN COUNT<DATA ELSE 
--			'1' WHEN DATA=MAX ELSE '0';
--
--
--end PWM_CLOCK_DIVIDER;
----BUFFER_REG_N--
