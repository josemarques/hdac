-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "12/24/2023 21:43:53"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          HDAC
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY HDAC_vhd_vec_tst IS
END HDAC_vhd_vec_tst;
ARCHITECTURE HDAC_arch OF HDAC_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL CLK : STD_LOGIC;
SIGNAL DATA : STD_LOGIC_VECTOR(11 DOWNTO 0);
SIGNAL R2R_0 : STD_LOGIC_VECTOR(7 DOWNTO 0);
COMPONENT HDAC
	PORT (
	CLK : IN STD_LOGIC;
	DATA : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
	R2R_0 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;
BEGIN
	i1 : HDAC
	PORT MAP (
-- list connections between master ports and signals
	CLK => CLK,
	DATA => DATA,
	R2R_0 => R2R_0
	);

-- CLK
t_prcs_CLK: PROCESS
BEGIN
LOOP
	CLK <= '0';
	WAIT FOR 5000 ps;
	CLK <= '1';
	WAIT FOR 5000 ps;
	IF (NOW >= 1000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_CLK;
-- DATA[11]
t_prcs_DATA_11: PROCESS
BEGIN
	DATA(11) <= '1';
WAIT;
END PROCESS t_prcs_DATA_11;
-- DATA[10]
t_prcs_DATA_10: PROCESS
BEGIN
	DATA(10) <= '0';
WAIT;
END PROCESS t_prcs_DATA_10;
-- DATA[9]
t_prcs_DATA_9: PROCESS
BEGIN
	DATA(9) <= '0';
WAIT;
END PROCESS t_prcs_DATA_9;
-- DATA[8]
t_prcs_DATA_8: PROCESS
BEGIN
	DATA(8) <= '0';
WAIT;
END PROCESS t_prcs_DATA_8;
-- DATA[7]
t_prcs_DATA_7: PROCESS
BEGIN
	DATA(7) <= '0';
WAIT;
END PROCESS t_prcs_DATA_7;
-- DATA[6]
t_prcs_DATA_6: PROCESS
BEGIN
	DATA(6) <= '0';
WAIT;
END PROCESS t_prcs_DATA_6;
-- DATA[5]
t_prcs_DATA_5: PROCESS
BEGIN
	DATA(5) <= '1';
WAIT;
END PROCESS t_prcs_DATA_5;
-- DATA[4]
t_prcs_DATA_4: PROCESS
BEGIN
	DATA(4) <= '1';
WAIT;
END PROCESS t_prcs_DATA_4;
-- DATA[3]
t_prcs_DATA_3: PROCESS
BEGIN
	DATA(3) <= '1';
WAIT;
END PROCESS t_prcs_DATA_3;
-- DATA[2]
t_prcs_DATA_2: PROCESS
BEGIN
	DATA(2) <= '1';
WAIT;
END PROCESS t_prcs_DATA_2;
-- DATA[1]
t_prcs_DATA_1: PROCESS
BEGIN
	DATA(1) <= '1';
WAIT;
END PROCESS t_prcs_DATA_1;
-- DATA[0]
t_prcs_DATA_0: PROCESS
BEGIN
	DATA(0) <= '1';
WAIT;
END PROCESS t_prcs_DATA_0;
END HDAC_arch;
