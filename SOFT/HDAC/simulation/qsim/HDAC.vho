-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 22.1std.1 Build 917 02/14/2023 SC Lite Edition"

-- DATE "12/24/2023 21:43:55"

-- 
-- Device: Altera EPM570F100I5 Package FBGA100
-- 

-- 
-- This VHDL file should be used for Questa Intel FPGA (VHDL) only
-- 

LIBRARY IEEE;
LIBRARY MAXII;
USE IEEE.STD_LOGIC_1164.ALL;
USE MAXII.MAXII_COMPONENTS.ALL;

ENTITY 	HDAC IS
    PORT (
	R2R_0 : OUT std_logic_vector(7 DOWNTO 0);
	DATA : IN std_logic_vector(11 DOWNTO 0);
	CLK : IN std_logic
	);
END HDAC;

-- Design Ports Information


ARCHITECTURE structure OF HDAC IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_R2R_0 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_DATA : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_CLK : std_logic;
SIGNAL \CLK~combout\ : std_logic;
SIGNAL \inst2|process_0~0_combout\ : std_logic;
SIGNAL \inst2|process_0~1_combout\ : std_logic;
SIGNAL \inst2|process_0~4_combout\ : std_logic;
SIGNAL \inst2|process_0~2_combout\ : std_logic;
SIGNAL \inst2|process_0~3_combout\ : std_logic;
SIGNAL \inst2|COUNT[0]~1\ : std_logic;
SIGNAL \inst2|COUNT[0]~1COUT1_11\ : std_logic;
SIGNAL \inst2|COUNT[1]~5\ : std_logic;
SIGNAL \inst2|COUNT[1]~5COUT1_12\ : std_logic;
SIGNAL \inst2|COUNT[2]~3\ : std_logic;
SIGNAL \inst2|COUNT[2]~3COUT1_13\ : std_logic;
SIGNAL \inst2|COUNT[3]~7\ : std_logic;
SIGNAL \inst2|COUNT[3]~7COUT1_14\ : std_logic;
SIGNAL \inst2|PWM~3_combout\ : std_logic;
SIGNAL \inst2|PWM~0_combout\ : std_logic;
SIGNAL \inst2|PWM~1_combout\ : std_logic;
SIGNAL \inst2|PWM~2_combout\ : std_logic;
SIGNAL \inst2|PWM~4_combout\ : std_logic;
SIGNAL \inst2|PWM~5_combout\ : std_logic;
SIGNAL \inst2|PWM~6_combout\ : std_logic;
SIGNAL \inst2|INCREM\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \DATA~combout\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \inst2|COUNT\ : std_logic_vector(4 DOWNTO 0);

BEGIN

R2R_0 <= ww_R2R_0;
ww_DATA <= DATA;
ww_CLK <= CLK;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: PIN_D9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[11]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(11),
	combout => \DATA~combout\(11));

-- Location: PIN_B8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[10]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(10),
	combout => \DATA~combout\(10));

-- Location: PIN_J10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[9]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(9),
	combout => \DATA~combout\(9));

-- Location: PIN_C4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[8]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(8),
	combout => \DATA~combout\(8));

-- Location: PIN_G8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[7]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(7),
	combout => \DATA~combout\(7));

-- Location: PIN_K8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[6]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(6),
	combout => \DATA~combout\(6));

-- Location: PIN_E10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[5]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(5),
	combout => \DATA~combout\(5));

-- Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLK~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_CLK,
	combout => \CLK~combout\);

-- Location: PIN_A7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[0]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(0),
	combout => \DATA~combout\(0));

-- Location: PIN_K9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[4]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(4),
	combout => \DATA~combout\(4));

-- Location: PIN_B5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[1]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(1),
	combout => \DATA~combout\(1));

-- Location: PIN_B6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[2]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(2),
	combout => \DATA~combout\(2));

-- Location: PIN_B7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[3]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(3),
	combout => \DATA~combout\(3));

-- Location: LC_X9_Y7_N1
\inst2|process_0~0\ : maxii_lcell
-- Equation(s):
-- \inst2|process_0~0_combout\ = (\DATA~combout\(1)) # ((\DATA~combout\(2)) # ((\DATA~combout\(3)) # (\DATA~combout\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(1),
	datab => \DATA~combout\(2),
	datac => \DATA~combout\(3),
	datad => \DATA~combout\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|process_0~0_combout\);

-- Location: LC_X10_Y3_N2
\inst2|process_0~1\ : maxii_lcell
-- Equation(s):
-- \inst2|process_0~1_combout\ = ((\DATA~combout\(4)) # ((\inst2|process_0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fcfc",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \DATA~combout\(4),
	datac => \inst2|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|process_0~1_combout\);

-- Location: LC_X7_Y7_N8
\inst2|INCREM[0]\ : maxii_lcell
-- Equation(s):
-- \inst2|INCREM\(0) = ((GLOBAL(\inst2|process_0~1_combout\) & (\DATA~combout\(0))) # (!GLOBAL(\inst2|process_0~1_combout\) & ((\inst2|INCREM\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "cfc0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \DATA~combout\(0),
	datac => \inst2|process_0~1_combout\,
	datad => \inst2|INCREM\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|INCREM\(0));

-- Location: LC_X7_Y7_N0
\inst2|COUNT[0]\ : maxii_lcell
-- Equation(s):
-- \inst2|COUNT\(0) = DFFEAS(\inst2|INCREM\(0) $ ((\inst2|COUNT\(0))), GLOBAL(\CLK~combout\), VCC, , , , , , )
-- \inst2|COUNT[0]~1\ = CARRY((\inst2|INCREM\(0) & (\inst2|COUNT\(0))))
-- \inst2|COUNT[0]~1COUT1_11\ = CARRY((\inst2|INCREM\(0) & (\inst2|COUNT\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => \inst2|INCREM\(0),
	datab => \inst2|COUNT\(0),
	aclr => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst2|COUNT\(0),
	cout0 => \inst2|COUNT[0]~1\,
	cout1 => \inst2|COUNT[0]~1COUT1_11\);

-- Location: LC_X9_Y7_N8
\inst2|INCREM[4]\ : maxii_lcell
-- Equation(s):
-- \inst2|INCREM\(4) = ((GLOBAL(\inst2|process_0~1_combout\) & (!\inst2|process_0~0_combout\)) # (!GLOBAL(\inst2|process_0~1_combout\) & ((\inst2|INCREM\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5f50",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|process_0~0_combout\,
	datac => \inst2|process_0~1_combout\,
	datad => \inst2|INCREM\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|INCREM\(4));

-- Location: LC_X8_Y7_N0
\inst2|process_0~4\ : maxii_lcell
-- Equation(s):
-- \inst2|process_0~4_combout\ = (!\DATA~combout\(0) & (\DATA~combout\(3) & (!\DATA~combout\(2) & !\DATA~combout\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0004",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(0),
	datab => \DATA~combout\(3),
	datac => \DATA~combout\(2),
	datad => \DATA~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|process_0~4_combout\);

-- Location: LC_X7_Y7_N9
\inst2|INCREM[3]\ : maxii_lcell
-- Equation(s):
-- \inst2|INCREM\(3) = ((GLOBAL(\inst2|process_0~1_combout\) & (\inst2|process_0~4_combout\)) # (!GLOBAL(\inst2|process_0~1_combout\) & ((\inst2|INCREM\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "afa0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|process_0~4_combout\,
	datac => \inst2|process_0~1_combout\,
	datad => \inst2|INCREM\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|INCREM\(3));

-- Location: LC_X8_Y7_N1
\inst2|process_0~2\ : maxii_lcell
-- Equation(s):
-- \inst2|process_0~2_combout\ = ((!\DATA~combout\(0) & (\DATA~combout\(2) & !\DATA~combout\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0030",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \DATA~combout\(0),
	datac => \DATA~combout\(2),
	datad => \DATA~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|process_0~2_combout\);

-- Location: LC_X7_Y7_N6
\inst2|INCREM[2]\ : maxii_lcell
-- Equation(s):
-- \inst2|INCREM\(2) = ((GLOBAL(\inst2|process_0~1_combout\) & (\inst2|process_0~2_combout\)) # (!GLOBAL(\inst2|process_0~1_combout\) & ((\inst2|INCREM\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "cfc0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|process_0~2_combout\,
	datac => \inst2|process_0~1_combout\,
	datad => \inst2|INCREM\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|INCREM\(2));

-- Location: LC_X7_Y7_N7
\inst2|process_0~3\ : maxii_lcell
-- Equation(s):
-- \inst2|process_0~3_combout\ = (!\DATA~combout\(0) & (((\DATA~combout\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5500",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(0),
	datad => \DATA~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|process_0~3_combout\);

-- Location: LC_X7_Y7_N5
\inst2|INCREM[1]\ : maxii_lcell
-- Equation(s):
-- \inst2|INCREM\(1) = (GLOBAL(\inst2|process_0~1_combout\) & (((\inst2|process_0~3_combout\)))) # (!GLOBAL(\inst2|process_0~1_combout\) & (\inst2|INCREM\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "caca",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|INCREM\(1),
	datab => \inst2|process_0~3_combout\,
	datac => \inst2|process_0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|INCREM\(1));

-- Location: LC_X7_Y7_N1
\inst2|COUNT[1]\ : maxii_lcell
-- Equation(s):
-- \inst2|COUNT\(1) = DFFEAS(\inst2|INCREM\(1) $ (\inst2|COUNT\(1) $ ((\inst2|COUNT[0]~1\))), GLOBAL(\CLK~combout\), VCC, , , , , , )
-- \inst2|COUNT[1]~5\ = CARRY((\inst2|INCREM\(1) & (!\inst2|COUNT\(1) & !\inst2|COUNT[0]~1\)) # (!\inst2|INCREM\(1) & ((!\inst2|COUNT[0]~1\) # (!\inst2|COUNT\(1)))))
-- \inst2|COUNT[1]~5COUT1_12\ = CARRY((\inst2|INCREM\(1) & (!\inst2|COUNT\(1) & !\inst2|COUNT[0]~1COUT1_11\)) # (!\inst2|INCREM\(1) & ((!\inst2|COUNT[0]~1COUT1_11\) # (!\inst2|COUNT\(1)))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => \inst2|INCREM\(1),
	datab => \inst2|COUNT\(1),
	aclr => GND,
	cin0 => \inst2|COUNT[0]~1\,
	cin1 => \inst2|COUNT[0]~1COUT1_11\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst2|COUNT\(1),
	cout0 => \inst2|COUNT[1]~5\,
	cout1 => \inst2|COUNT[1]~5COUT1_12\);

-- Location: LC_X7_Y7_N2
\inst2|COUNT[2]\ : maxii_lcell
-- Equation(s):
-- \inst2|COUNT\(2) = DFFEAS(\inst2|INCREM\(2) $ (\inst2|COUNT\(2) $ ((!\inst2|COUNT[1]~5\))), GLOBAL(\CLK~combout\), VCC, , , , , , )
-- \inst2|COUNT[2]~3\ = CARRY((\inst2|INCREM\(2) & ((\inst2|COUNT\(2)) # (!\inst2|COUNT[1]~5\))) # (!\inst2|INCREM\(2) & (\inst2|COUNT\(2) & !\inst2|COUNT[1]~5\)))
-- \inst2|COUNT[2]~3COUT1_13\ = CARRY((\inst2|INCREM\(2) & ((\inst2|COUNT\(2)) # (!\inst2|COUNT[1]~5COUT1_12\))) # (!\inst2|INCREM\(2) & (\inst2|COUNT\(2) & !\inst2|COUNT[1]~5COUT1_12\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => \inst2|INCREM\(2),
	datab => \inst2|COUNT\(2),
	aclr => GND,
	cin0 => \inst2|COUNT[1]~5\,
	cin1 => \inst2|COUNT[1]~5COUT1_12\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst2|COUNT\(2),
	cout0 => \inst2|COUNT[2]~3\,
	cout1 => \inst2|COUNT[2]~3COUT1_13\);

-- Location: LC_X7_Y7_N3
\inst2|COUNT[3]\ : maxii_lcell
-- Equation(s):
-- \inst2|COUNT\(3) = DFFEAS(\inst2|COUNT\(3) $ (\inst2|INCREM\(3) $ ((\inst2|COUNT[2]~3\))), GLOBAL(\CLK~combout\), VCC, , , , , , )
-- \inst2|COUNT[3]~7\ = CARRY((\inst2|COUNT\(3) & (!\inst2|INCREM\(3) & !\inst2|COUNT[2]~3\)) # (!\inst2|COUNT\(3) & ((!\inst2|COUNT[2]~3\) # (!\inst2|INCREM\(3)))))
-- \inst2|COUNT[3]~7COUT1_14\ = CARRY((\inst2|COUNT\(3) & (!\inst2|INCREM\(3) & !\inst2|COUNT[2]~3COUT1_13\)) # (!\inst2|COUNT\(3) & ((!\inst2|COUNT[2]~3COUT1_13\) # (!\inst2|INCREM\(3)))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => \inst2|COUNT\(3),
	datab => \inst2|INCREM\(3),
	aclr => GND,
	cin0 => \inst2|COUNT[2]~3\,
	cin1 => \inst2|COUNT[2]~3COUT1_13\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst2|COUNT\(3),
	cout0 => \inst2|COUNT[3]~7\,
	cout1 => \inst2|COUNT[3]~7COUT1_14\);

-- Location: LC_X7_Y7_N4
\inst2|COUNT[4]\ : maxii_lcell
-- Equation(s):
-- \inst2|COUNT\(4) = DFFEAS(\inst2|COUNT\(4) $ (((\inst2|COUNT[3]~7\ $ (!\inst2|INCREM\(4))))), GLOBAL(\CLK~combout\), VCC, , , , , , )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5aa5",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => \inst2|COUNT\(4),
	datad => \inst2|INCREM\(4),
	aclr => GND,
	cin0 => \inst2|COUNT[3]~7\,
	cin1 => \inst2|COUNT[3]~7COUT1_14\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst2|COUNT\(4));

-- Location: LC_X9_Y7_N7
\inst2|PWM~3\ : maxii_lcell
-- Equation(s):
-- \inst2|PWM~3_combout\ = (\inst2|COUNT\(4) & (((\inst2|COUNT\(3)) # (!\DATA~combout\(4))))) # (!\inst2|COUNT\(4) & (!\DATA~combout\(3) & (\inst2|COUNT\(3) & !\DATA~combout\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "c0dc",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(3),
	datab => \inst2|COUNT\(4),
	datac => \inst2|COUNT\(3),
	datad => \DATA~combout\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|PWM~3_combout\);

-- Location: LC_X8_Y7_N2
\inst2|PWM~0\ : maxii_lcell
-- Equation(s):
-- \inst2|PWM~0_combout\ = (\inst2|COUNT\(2) & (((\DATA~combout\(2) & \DATA~combout\(1))))) # (!\inst2|COUNT\(2) & ((\DATA~combout\(2)) # ((!\inst2|COUNT\(1) & \DATA~combout\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f130",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|COUNT\(1),
	datab => \inst2|COUNT\(2),
	datac => \DATA~combout\(2),
	datad => \DATA~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|PWM~0_combout\);

-- Location: LC_X8_Y7_N5
\inst2|PWM~1\ : maxii_lcell
-- Equation(s):
-- \inst2|PWM~1_combout\ = (\inst2|COUNT\(1) & ((\inst2|COUNT\(2)) # ((!\DATA~combout\(2) & !\DATA~combout\(1))))) # (!\inst2|COUNT\(1) & (\inst2|COUNT\(2) & (!\DATA~combout\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8c8e",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|COUNT\(1),
	datab => \inst2|COUNT\(2),
	datac => \DATA~combout\(2),
	datad => \DATA~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|PWM~1_combout\);

-- Location: LC_X9_Y7_N9
\inst2|PWM~2\ : maxii_lcell
-- Equation(s):
-- \inst2|PWM~2_combout\ = (\DATA~combout\(3) & ((\DATA~combout\(4)) # ((!\inst2|COUNT\(4) & !\inst2|COUNT\(3))))) # (!\DATA~combout\(3) & (!\inst2|COUNT\(4) & ((\DATA~combout\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "bb02",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(3),
	datab => \inst2|COUNT\(4),
	datac => \inst2|COUNT\(3),
	datad => \DATA~combout\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|PWM~2_combout\);

-- Location: LC_X8_Y7_N9
\inst2|PWM~4\ : maxii_lcell
-- Equation(s):
-- \inst2|PWM~4_combout\ = (\inst2|PWM~3_combout\ & (\inst2|PWM~0_combout\ & ((\inst2|PWM~2_combout\)))) # (!\inst2|PWM~3_combout\ & ((\inst2|PWM~2_combout\) # ((\inst2|PWM~0_combout\ & !\inst2|PWM~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "dd04",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|PWM~3_combout\,
	datab => \inst2|PWM~0_combout\,
	datac => \inst2|PWM~1_combout\,
	datad => \inst2|PWM~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|PWM~4_combout\);

-- Location: LC_X8_Y7_N3
\inst2|PWM~5\ : maxii_lcell
-- Equation(s):
-- \inst2|PWM~5_combout\ = (\inst2|PWM~3_combout\ & (((\inst2|PWM~1_combout\) # (!\inst2|PWM~2_combout\)))) # (!\inst2|PWM~3_combout\ & (!\inst2|PWM~0_combout\ & (\inst2|PWM~1_combout\ & !\inst2|PWM~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "a0ba",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|PWM~3_combout\,
	datab => \inst2|PWM~0_combout\,
	datac => \inst2|PWM~1_combout\,
	datad => \inst2|PWM~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|PWM~5_combout\);

-- Location: LC_X8_Y7_N7
\inst2|PWM~6\ : maxii_lcell
-- Equation(s):
-- \inst2|PWM~6_combout\ = (\inst2|PWM~4_combout\ & (((\DATA~combout\(0)) # (!\inst2|PWM~5_combout\)))) # (!\inst2|PWM~4_combout\ & (!\inst2|COUNT\(0) & (\DATA~combout\(0) & !\inst2|PWM~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "c0dc",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|COUNT\(0),
	datab => \inst2|PWM~4_combout\,
	datac => \DATA~combout\(0),
	datad => \inst2|PWM~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst2|PWM~6_combout\);

-- Location: PIN_D8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\R2R_0[7]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \DATA~combout\(11),
	oe => VCC,
	padio => ww_R2R_0(7));

-- Location: PIN_A9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\R2R_0[6]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \DATA~combout\(10),
	oe => VCC,
	padio => ww_R2R_0(6));

-- Location: PIN_H10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\R2R_0[5]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \DATA~combout\(9),
	oe => VCC,
	padio => ww_R2R_0(5));

-- Location: PIN_A3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\R2R_0[4]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \DATA~combout\(8),
	oe => VCC,
	padio => ww_R2R_0(4));

-- Location: PIN_F10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\R2R_0[3]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \DATA~combout\(7),
	oe => VCC,
	padio => ww_R2R_0(3));

-- Location: PIN_K6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\R2R_0[2]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \DATA~combout\(6),
	oe => VCC,
	padio => ww_R2R_0(2));

-- Location: PIN_F8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\R2R_0[1]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \DATA~combout\(5),
	oe => VCC,
	padio => ww_R2R_0(1));

-- Location: PIN_A6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\R2R_0[0]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst2|PWM~6_combout\,
	oe => VCC,
	padio => ww_R2R_0(0));
END structure;


